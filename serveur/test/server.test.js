const request = require('supertest');
const app = require('../server.js');

describe('Tests des routes', () => {
  // Test de la route GET /produits
  it('Devrait retourner la liste des produits', async () => {
    const response = await request(app).get('/produits');
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route GET /produits/orderByPrix
  it('Devrait retourner la liste des produits triés par prix décroissant', async () => {
    const response = await request(app).get('/produits/orderByPrix');
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route GET /produits/orderByNom
  it('Devrait retourner la liste des produits triés par nom décroissant', async () => {
    const response = await request(app).get('/produits/orderByNom');
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route GET /produit/:ref
  it('Devrait retourner un produit en fonction de sa référence', async () => {
    const response = await request(app).get('/produit/REF123');
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route POST /produit/add
  it('Devrait ajouter un produit', async () => {
    const response = await request(app)
      .post('/produit/add')
      .send({
        reference: 'REF456',
        nomProduit: 'Produit 456',
        description: 'Description du produit 456',
        prix: 9.99,
        quantite: 10,
      });
    expect(response.status).toBe(200);
    expect(response.text).toBe('Produit ajouté avec succès');
  });

  // Test de la route PUT /produit/:ref
  it('Devrait mettre à jour un produit en fonction de sa référence', async () => {
    const response = await request(app)
      .put('/produit/REF456')
      .send({
        reference: 'REF456',
        nomProduit: 'Produit 456 mis à jour',
        description: 'Description du produit 456 mise à jour',
        prix: 19.99,
        quantite: 5,
      });
    expect(response.status).toBe(200);
    expect(response.text).toBe('Produit mis à jour avec succès');
  });

  // Test de la route DELETE /produit/:ref
  it('Devrait supprimer un produit en fonction de sa référence', async () => {
    const response = await request(app).delete('/produit/REF456');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Produit supprimé avec succès');
  });

  // Test de la route GET /user
  it('Devrait retourner la liste des utilisateurs', async () => {
    const response = await request(app).get('/user');
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route POST /user/new
  it('Devrait créer un nouvel utilisateur', async () => {
    const response = await request(app)
      .post('/user/new')
      .send({
        nomUtilisateur: 'Utilisateur123',
        mdp: 'MotDePasse123',
        role: 'utilisateur',
        mail: 'utilisateur123@example.com',
        prenomUtilisateur: 'Utilisateur',
      });
    expect(response.status).toBe(200);
    expect(response.text).toBe("Ajout avec succès d'un utilisateur");
  });

  // Test de la route POST /login
  it('Devrait permettre à un utilisateur de se connecter', async () => {
    const response = await request(app)
      .post('/login')
      .send({
        mail: 'utilisateur123@example.com',
        mdp: 'MotDePasse123',
      });
    expect(response.status).toBe(200);
    expect(response.body).toBeDefined();
  });

  // Test de la route PUT /user/:id
  it('Devrait mettre à jour un utilisateur en fonction de son ID', async () => {
    const response = await request(app)
      .put('/user/123')
      .send({
        nomUtilisateur: 'Utilisateur123 mis à jour',
        prenomUtilisateur: 'Utilisateur mis à jour',
        mdp: 'NouveauMotDePasse123',
        mail: 'utilisateur123@example.com',
        role: 'utilisateur',
      });
    expect(response.status).toBe(200);
    expect(response.text).toBe("Modification de l'utilisateur avec succès");
  });

  // Test de la route DELETE /user/:id
  it('Devrait supprimer un utilisateur en fonction de son ID', async () => {
    const response = await request(app).delete('/user/123');
    expect(response.status).toBe(200);
    expect(response.text).toBe("Suppression de l'utilisateur avec succès");
  });
});