const mariadb = require("mariadb");
const express = require("express");
const app = express();
var cors = require("cors");
var bcrypt = require("bcryptjs");

require("dotenv").config();

const pool = mariadb.createPool({
  host: process.env.DB_HOST,
  database : process.env.DB_DTB,
  user : process.env.DB_USER,
  password : process.env.DB_PWD
})

// Utilisation du module express
app.use(express.json());
app.use(cors());

// Récupérer la liste des produits
app.get("/produits", async (req, res) => {
  let conn;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit;");
  res.status(200).json(rows);
  conn.release();
});

app.get("/produits/orderByPrix", async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM produit ORDER BY prix ASC ;");
    res.status(200).json(rows);
    conn.release();
}); 

app.get("/produits/orderByNom", async (req, res) => {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM produit ORDER BY nomProduit ASC ;");
    res.status(200).json(rows);
    conn.release();
});

// Récupérer un produit avec sa référence
app.get("/produit/:ref", async (req, res) => {
  let conn;
  let ref = req.params.ref;
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM produit WHERE reference = ?;", [
    ref,
  ]);
  res.status(200).json(rows);
  conn.release();
});

// Ajouter un produit
app.post("/produit/add", async (req, res) => {
  try {
    let conn;
    conn = await pool.getConnection();
    await conn.query(
      "INSERT INTO produit(reference, nomProduit, description, prix, quantite) VALUES (?,?,?,?,?);",
      [
        req.body.reference,
        req.body.nomProduit,
        req.body.description,
        req.body.prix,
        req.body.quantite,
      ]
    );
    res.status(200).send("Produit ajouté avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'ajout d'un produit");
    conn.release();
  }
});

// Modifier un produit avec sa référence
app.put("/produit/:ref", async (req, res) => {
  try {
    let conn;
    let ref = req.params.ref;
    conn = await pool.getConnection();
    await conn.query(
      "UPDATE produit SET reference = ?, nomProduit = ?, description = ?, prix = ?, quantite = ? WHERE reference = ?",
      [
        req.body.reference,
        req.body.nomProduit,
        req.body.description,
        req.body.prix,
        req.body.quantite,
        ref,
      ]
    );
    res.status(200).send("Produit mis à jour avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Une erreur est survenue lors de la mise à jour");
    if (conn) conn.release();
  }
});


// Suppression d'un produit avec sa référence
app.delete("/produit/:ref", async (req, res) => {
  try {
    let conn;
    let ref = req.params.ref;
    conn = await pool.getConnection();
    await conn.query(`DELETE FROM produit WHERE reference = ?;`, [ref]);
    res.status(200).send("Produit supprimé avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).json({message : "Erreur de suppression du produit"});
    if (conn) conn.release();
  }
});

// Ajouter une commande
app.post("/commande/add", async (req, res) => {
  let conn; // Déclarer la variable ici

  try {
    conn = await pool.getConnection();

    // Sérialiser le BigInt en chaîne de caractères
    const idUtilisateur = req.body.idUtilisateur.toString(10);

    const result = await conn.query(
      "INSERT INTO commande(dateCommande, nbArticle, total, quantite, idUtilisateur) VALUES (?, ?, ?, ?, ?);",
      [
        req.body.dateCommande,
        req.body.nbArticle,
        req.body.total,
        req.body.quantite,
        idUtilisateur,
      ]
    );
    res.status(200).json({ id: result.insertId.toString(10) }); // Convertir également le BigInt en chaîne de caractères ici
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'ajout de la commande");
  } finally {
    if (conn) {
      conn.release();
    }
  }
});

// Obtenir l'historique des commandes de l'utilisateur connecté
app.get("/commande/history/:id", async (req, res) => {
  let conn;

  try {
    const userId = req.params.id;

    conn = await pool.getConnection();

    const result = await conn.query(
      "SELECT * FROM commande WHERE idUtilisateur = ?",
      [userId]
    );

    res.status(200).json(result);
  } catch (err) {
    console.log(err);
    res.status(500).send("Erreur lors de la récupération de l'historique des commandes");
  } finally {
    if (conn) {
      conn.release();
    }
  }
});



// Voir la liste de tous les utilisateurs
app.get("/user", async (req, res) => {
  try {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur;");
    res.status(200).json(rows);
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de la récupération de la liste des utilisateurs");
    conn.release();
  }
});

// Créer un nouvelle utilisateur
app.post("/user/new", async (req, res) => {
  var hash = bcrypt.hashSync(req.body.mdp, 8);
  try {
    let conn;
    conn = await pool.getConnection();
    await conn.query(
      "INSERT INTO utilisateur(nomUtilisateur, mdp, dateCreation, role, mail, prenomUtilisateur) VALUES (?,?,NOW(),?,?,?)",
      [
        req.body.nomUtilisateur,
        hash,
        req.body.role,
        req.body.mail,
        req.body.prenomUtilisateur,
      ]
    );
    res.status(200).send("Ajout avec succès d'un utilisateur");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de l'ajout d'un utilisateur");
    conn.release();
  }
});


// Se connecter à son compte
app.post("/login", async (req, res) => {
  try {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur WHERE mail = ?;", [
      req.body.mail,
    ]);
    if (rows.length == 0) {
      // utilisateur n'existe pas
      res.status(400).send("Email ou mot de passe incorrect.");
    } else {
      // vérification du mot de passe
      const hash = rows[0].mdp;
      const isValidPassword = await bcrypt.compare(req.body.mdp, hash);
      if (!isValidPassword) {
        res.status(400).send("Email ou mot de passe incorrect.");
      } else {
        // connexion réussie
        res.status(200).json(rows[0]);
      }
    }
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de connexion au compte");
    conn.release();
  }
});

// Modifier un utilisateur avec son id
app.put("/user/:id", async (req, res) => {
  try {
    let conn;
    let id = req.params.id;
    conn = await pool.getConnection();
    await conn.query(
      `UPDATE utilisateur SET nomUtilisateur = ?, prenomUtilisateur = ?, mdp = ?, mail = ?,  role = ? WHERE id = ${id};`,
      [req.body.nomUtilisateur, req.body.prenomUtilisateur,req.body.mdp, req.body.mail, req.body.role]
    );
    res.status(200).send("Modification de l'utilisateur avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur lors de la modification de l'utilistateur");
    conn.release();
  }
});

// Supprimer un utilisateur avec son id
app.delete("/user/:id", async (req, res) => {
  try {
    let conn;
    let id = req.params.id;
    conn = await pool.getConnection();
    await conn.query(`DELETE FROM utilisateur WHERE id = ?;`, [id]);
    res.status(200).send("Suppression de l'utilisateur avec succès");
    conn.release();
  } catch (err) {
    console.log(err);
    res.status(400).send("Erreur de suppression de l'utilisateur");
    conn.release();
  }
});

app.get("/user/email/:email", async (req, res) => {
  const email = req.params.email;

  try {
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM utilisateur WHERE mail = ?;", [email]);

    if (rows.length > 0) {
      res.json({ exists: true });
    } else {
      res.json({ exists: false });
    }

    conn.release();
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Une erreur s'est produite lors de la vérification de l'e-mail." });
    conn.release();
  }
});

app.listen(8000, () => {
  console.log("Serveur à l'écoute");
});

module.exports = app;
