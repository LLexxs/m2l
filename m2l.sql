-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 27 avr. 2023 à 15:51
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `m2l`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Sports'),
(2, 'Jeux'),
(3, 'Autres');

-- --------------------------------------------------------

--
-- Structure de la table `categorieproduit`
--

CREATE TABLE `categorieproduit` (
  `categorieId` int(11) DEFAULT NULL,
  `produitReference` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorieproduit`
--

INSERT INTO `categorieproduit` (`categorieId`, `produitReference`) VALUES
(1, 'P00001'),
(1, 'P00005'),
(1, 'P00009'),
(2, 'P00006'),
(2, 'P00011'),
(3, 'P00002'),
(3, 'P00003'),
(3, 'P00004'),
(3, 'P00007'),
(3, 'P00008'),
(3, 'P00010'),
(3, 'P00012');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `dateCommande` date DEFAULT NULL,
  `nbArticle` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `idUtilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `dateCommande`, `nbArticle`, `total`, `quantite`, `idUtilisateur`) VALUES
(1, '2022-01-01', 3, 50.99, 1, 2),
(2, '2022-01-02', 2, 30.99, 2, 3),
(3, '2022-01-03', 5, 120.99, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `reference` varchar(100) NOT NULL,
  `nomProduit` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `prix` float DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`reference`, `nomProduit`, `description`, `prix`, `quantite`) VALUES
('P00001', 'Balle de foot', 'Balle de football classique pour tous les niveaux de jeu', 20.99, 50),
('P00002', 'Balle de golf', 'Balle de golf à deux pièces pour les golfeurs débutants et intermédiaires', 25.99, 30),
('P00003', 'Balle de baseball', 'Balle de baseball en cuir pour la pratique du baseball', 12.99, 60),
('P00004', 'Balle de tennis', 'Balle de tennis de haute qualité pour les matchs professionnels', 8.99, 80),
('P00005', 'Balle de basket', 'Balle de basket-ball officielle de la NBA', 34.99, 30),
('P00006', 'Balle de bowling', 'Balle de bowling de haute qualité pour les joueurs expérimentés', 15.99, 40),
('P00007', 'Balle de criket', 'Balle de cricket de haute qualité pour les matchs professionnels', 22.99, 20),
('P00008', 'Balle de plage', 'Balle de plage de haute qualité pour les jeux de plage', 9.99, 100),
('P00009', 'Balle de rugby', 'Balle de rugby légère et résistante pour une utilisation en compétition', 25.99, 40),
('P00010', 'Boule de billard', 'Boule de billard standard pour tous les niveaux de jeu', 4.99, 120),
('P00011', 'Palet de hockey', 'Palet de hockey sur glace pour les joueurs professionnels', 7.99, 50),
('P00012', 'Balle de volley', 'Balle de volley-ball de haute qualité pour les matchs professionnels', 27.99, 25);

-- --------------------------------------------------------

--
-- Structure de la table `produitcommande`
--

CREATE TABLE `produitcommande` (
  `commandeId` int(11) DEFAULT NULL,
  `produitReference` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produitcommande`
--

INSERT INTO `produitcommande` (`commandeId`, `produitReference`) VALUES
(1, 'P00001'),
(1, 'P00005'),
(1, 'P00009'),
(2, 'P00006'),
(2, 'P00010'),
(3, 'P00002'),
(3, 'P00004'),
(3, 'P00007'),
(3, 'P00009'),
(3, 'P00011'),
(3, 'P00012');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nomUtilisateur` varchar(100) DEFAULT NULL,
  `mdp` varchar(100) DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `prenomUtilisateur` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nomUtilisateur`, `mdp`, `dateCreation`, `role`, `mail`, `prenomUtilisateur`) VALUES
(1, 'user', '$2a$08$LkkxWD42f6JD/0CLEjTq0.htdT9KGUDxzBIvazzJQ0DwkaWoWDaqS', '2022-01-01', 'utilisateur', 'user@example.com', 'user'),
(2, 'adminproduit', '$2a$08$LkkxWD42f6JD/0CLEjTq0.htdT9KGUDxzBIvazzJQ0DwkaWoWDaqS', '2022-01-01', 'adminproduit', 'adminproduit@example.com', 'adminproduit'),
(3, 'admin', '$2a$08$LkkxWD42f6JD/0CLEjTq0.htdT9KGUDxzBIvazzJQ0DwkaWoWDaqS', '2022-01-01', 'admin', 'admin@example.com', 'admin');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categorieproduit`
--
ALTER TABLE `categorieproduit`
  ADD KEY `fk_categorieproduit_categorie` (`categorieId`),
  ADD KEY `fk_categorieproduit_produit` (`produitReference`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commande_utilisateur` (`idUtilisateur`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`reference`);

--
-- Index pour la table `produitcommande`
--
ALTER TABLE `produitcommande`
  ADD KEY `fk_produitcommande_commande` (`commandeId`),
  ADD KEY `fk_produitcommande_produit` (`produitReference`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categorieproduit`
--
ALTER TABLE `categorieproduit`
  ADD CONSTRAINT `fk_categorieproduit_categorie` FOREIGN KEY (`categorieId`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_categorieproduit_produit` FOREIGN KEY (`produitReference`) REFERENCES `produit` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_commande_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `produitcommande`
--
ALTER TABLE `produitcommande`
  ADD CONSTRAINT `fk_produitcommande_commande` FOREIGN KEY (`commandeId`) REFERENCES `commande` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_produitcommande_produit` FOREIGN KEY (`produitReference`) REFERENCES `produit` (`reference`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
