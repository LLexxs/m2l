# API Projet M2L

Ce projet est une API pour la Maison des Ligues de Lorraine (M2L), développée en Node.js.

## Installation

1. Cloner le repository :

```bash
git clone https://gitlab.com/LLexxs/m2l.git
```

2. Installer les dépendances :

```bash
cd m2l_back\m2l\serveur\
npm install
```

3. Démarrer le serveur :

```bash
node server.js
```

## Utilisation

L'API est accessible à l'adresse `http://localhost:8000` dans votre navigateur web ou via des requêtes HTTP.

## Endpoints

Voici la liste des endpoints disponibles :

| Endpoint                    | Description                                                                                     |
| ---------------------------|-------------------------------------------------------------------------------------------------|
| GET /produits             | Récupérer la liste des produits			                                                              |
| GET /produits/:ref         | Récupérer un produit en particulier                                                             |
| GET /produits/orderByPrix | Récupérer la liste des produits dans l'ordre décroissant des prix                        					  |
| GET /produits/orderByNom  | Récupérer la liste des produits dans l'ordre décroissant des noms                        						 |
| POST /produit/add            | Créer un nouveau produit                                                                        |
| PUT /produit/:ref         | Mettre à jour produit                                                                           |
| DELETE /produit/:ref      | Supprimer un produit                                                                            |
| GET /user					| Récupérer la liste des utilisateurs		                                                           |
| POST /user      | Créer un nouvel utilisateur                                                                     |
| PUT /user/:id   | Mettre à jour un utilisateur                                                                    |
| DELETE /user/:id| Supprimer un utilisateur                                                                        |

## Contribution

Les contributions sont les bienvenues ! Si vous souhaitez contribuer à ce projet, voici les étapes à suivre :

1. Forker le repository.
2. Déplacer-vous sur une branche pour votre contribution :
```bash
   git checkout main
   ```
3. Faire vos modifications et tester l'API localement.
4. Ajouter et commiter les modifications :
```bash
   git add .
   git commit -m "Description de votre contribution"
   ```
5. Pousser les modifications sur votre fork :
```bash
   git push origin main
   ```
6. Créer une pull request sur le repository original.
